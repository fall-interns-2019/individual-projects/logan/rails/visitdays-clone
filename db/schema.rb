# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_10_08_132720) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: :cascade do |t|
    t.string "name"
    t.string "resource_type"
    t.bigint "resource_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_admins_on_name_and_resource_type_and_resource_id"
    t.index ["resource_type", "resource_id"], name: "index_admins_on_resource_type_and_resource_id"
  end

  create_table "defaults", force: :cascade do |t|
    t.string "name"
    t.string "resource_type"
    t.bigint "resource_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_defaults_on_name_and_resource_type_and_resource_id"
    t.index ["resource_type", "resource_id"], name: "index_defaults_on_resource_type_and_resource_id"
  end

  create_table "event_organizers", force: :cascade do |t|
    t.bigint "event_id", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "archived", default: false
    t.index ["event_id"], name: "index_event_organizers_on_event_id"
    t.index ["user_id"], name: "index_event_organizers_on_user_id"
  end

  create_table "events", force: :cascade do |t|
    t.bigint "institution_id", null: false
    t.string "name"
    t.datetime "datetime"
    t.string "location"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "visit_type_id", null: false
    t.boolean "archived", default: false
    t.index ["institution_id"], name: "index_events_on_institution_id"
    t.index ["visit_type_id"], name: "index_events_on_visit_type_id"
  end

  create_table "institutions", force: :cascade do |t|
    t.string "name"
    t.string "address"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "events_count", default: 0
    t.integer "registrants_count", default: 0
    t.integer "surveys_count", default: 0
    t.integer "users_count", default: 0
    t.integer "visit_types_count", default: 0
    t.boolean "archived", default: false
  end

  create_table "managers", force: :cascade do |t|
    t.string "name"
    t.string "resource_type"
    t.bigint "resource_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_managers_on_name_and_resource_type_and_resource_id"
    t.index ["resource_type", "resource_id"], name: "index_managers_on_resource_type_and_resource_id"
  end

  create_table "members", force: :cascade do |t|
    t.string "name"
    t.string "resource_type"
    t.bigint "resource_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_members_on_name_and_resource_type_and_resource_id"
    t.index ["resource_type", "resource_id"], name: "index_members_on_resource_type_and_resource_id"
  end

  create_table "registrants", force: :cascade do |t|
    t.bigint "institution_id", null: false
    t.string "first_name"
    t.string "last_name"
    t.string "email"
    t.string "phone"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "archived", default: false
    t.index ["institution_id"], name: "index_registrants_on_institution_id"
  end

  create_table "registrations", force: :cascade do |t|
    t.bigint "registrant_id", null: false
    t.bigint "event_id", null: false
    t.integer "guest_count"
    t.datetime "registration_datetime"
    t.integer "status", default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "archived", default: false
    t.index ["event_id"], name: "index_registrations_on_event_id"
    t.index ["registrant_id"], name: "index_registrations_on_registrant_id"
  end

  create_table "survey_pages", force: :cascade do |t|
    t.bigint "survey_id", null: false
    t.integer "page"
    t.text "items_json"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "archived", default: false
    t.index ["survey_id"], name: "index_survey_pages_on_survey_id"
  end

  create_table "survey_responses", force: :cascade do |t|
    t.bigint "survey_id", null: false
    t.bigint "registrant_id", null: false
    t.text "responses_json"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "archived", default: false
    t.index ["registrant_id"], name: "index_survey_responses_on_registrant_id"
    t.index ["survey_id"], name: "index_survey_responses_on_survey_id"
  end

  create_table "surveys", force: :cascade do |t|
    t.bigint "institution_id", null: false
    t.bigint "user_id", null: false
    t.string "name"
    t.integer "type", default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "archived", default: false
    t.index ["institution_id"], name: "index_surveys_on_institution_id"
    t.index ["user_id"], name: "index_surveys_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "provider", default: "email", null: false
    t.string "uid", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.boolean "allow_password_change", default: false
    t.datetime "remember_created_at"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "first_name"
    t.string "last_name"
    t.string "email"
    t.text "tokens"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "institution_id"
    t.boolean "archived", default: false
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["institution_id"], name: "index_users_on_institution_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true
  end

  create_table "users_admins", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "admin_id"
    t.index ["admin_id"], name: "index_users_admins_on_admin_id"
    t.index ["user_id", "admin_id"], name: "index_users_admins_on_user_id_and_admin_id"
    t.index ["user_id"], name: "index_users_admins_on_user_id"
  end

  create_table "users_defaults", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "default_id"
    t.index ["default_id"], name: "index_users_defaults_on_default_id"
    t.index ["user_id", "default_id"], name: "index_users_defaults_on_user_id_and_default_id"
    t.index ["user_id"], name: "index_users_defaults_on_user_id"
  end

  create_table "users_managers", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "manager_id"
    t.index ["manager_id"], name: "index_users_managers_on_manager_id"
    t.index ["user_id", "manager_id"], name: "index_users_managers_on_user_id_and_manager_id"
    t.index ["user_id"], name: "index_users_managers_on_user_id"
  end

  create_table "users_members", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "member_id"
    t.index ["member_id"], name: "index_users_members_on_member_id"
    t.index ["user_id", "member_id"], name: "index_users_members_on_user_id_and_member_id"
    t.index ["user_id"], name: "index_users_members_on_user_id"
  end

  create_table "visit_types", force: :cascade do |t|
    t.bigint "institution_id", null: false
    t.string "name"
    t.datetime "date_start"
    t.datetime "date_end"
    t.text "times_json"
    t.string "location"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "archived", default: false
    t.index ["institution_id"], name: "index_visit_types_on_institution_id"
  end

  add_foreign_key "event_organizers", "events"
  add_foreign_key "event_organizers", "users"
  add_foreign_key "events", "institutions"
  add_foreign_key "events", "visit_types"
  add_foreign_key "registrants", "institutions"
  add_foreign_key "registrations", "events"
  add_foreign_key "registrations", "registrants"
  add_foreign_key "survey_pages", "surveys"
  add_foreign_key "survey_responses", "registrants"
  add_foreign_key "survey_responses", "surveys"
  add_foreign_key "surveys", "institutions"
  add_foreign_key "surveys", "users"
  add_foreign_key "users", "institutions"
  add_foreign_key "visit_types", "institutions"
end
