class CreateEvents < ActiveRecord::Migration[6.0]
  def change
    create_table :events do |t|
      t.references :institution, null: false, foreign_key: true
      t.string :name
      t.datetime :datetime
      t.string :location
      t.text :description

      t.timestamps
    end
  end
end
