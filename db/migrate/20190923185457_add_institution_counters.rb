class AddInstitutionCounters < ActiveRecord::Migration[6.0]
  def change
    add_column :institutions, :events_count, :integer, default: 0
    add_column :institutions, :registrants_count, :integer, default: 0
    add_column :institutions, :surveys_count, :integer, default: 0
    add_column :institutions, :users_count, :integer, default: 0
    add_column :institutions, :visit_types_count, :integer, default: 0
  end
end
