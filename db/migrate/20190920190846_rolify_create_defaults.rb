class RolifyCreateDefaults < ActiveRecord::Migration[6.0]
  def change
    create_table(:defaults) do |t|
      t.string :name
      t.references :resource, polymorphic: true

      t.timestamps
    end

    create_table(:users_defaults, id: false) do |t|
      t.references :user
      t.references :default
    end
    
    add_index(:defaults, [ :name, :resource_type, :resource_id ])
    add_index(:users_defaults, [ :user_id, :default_id ])
  end
end
