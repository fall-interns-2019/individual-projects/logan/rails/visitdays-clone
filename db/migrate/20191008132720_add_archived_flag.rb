class AddArchivedFlag < ActiveRecord::Migration[6.0]
  def change
    add_column :institutions, :archived, :boolean, default: false
    add_column :events, :archived, :boolean, default: false
    add_column :event_organizers, :archived, :boolean, default: false
    add_column :registrants, :archived, :boolean, default: false
    add_column :registrations, :archived, :boolean, default: false
    add_column :surveys, :archived, :boolean, default: false
    add_column :survey_pages, :archived, :boolean, default: false
    add_column :survey_responses, :archived, :boolean, default: false
    add_column :users, :archived, :boolean, default: false
    add_column :visit_types, :archived, :boolean, default: false
  end
end
