class CreateRegistrations < ActiveRecord::Migration[6.0]
  def change
    create_table :registrations do |t|
      t.references :registrant, null: false, foreign_key: true
      t.references :event, null: false, foreign_key: true
      t.integer :guest_count
      t.datetime :registration_datetime
      t.column :status, :integer, default: 0

      t.timestamps
    end
  end
end
