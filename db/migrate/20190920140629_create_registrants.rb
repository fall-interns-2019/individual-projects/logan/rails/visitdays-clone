class CreateRegistrants < ActiveRecord::Migration[6.0]
  def change
    create_table :registrants do |t|
      t.references :institution, null: false, foreign_key: true
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :phone

      t.timestamps
    end
  end
end
