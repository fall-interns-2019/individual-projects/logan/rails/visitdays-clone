class RolifyCreateMembers < ActiveRecord::Migration[6.0]
  def change
    create_table(:members) do |t|
      t.string :name
      t.references :resource, polymorphic: true

      t.timestamps
    end

    create_table(:users_members, id: false) do |t|
      t.references :user
      t.references :member
    end
    
    add_index(:members, %i[name resource_type resource_id])
    add_index(:users_members, %i[user_id member_id])
  end
end
