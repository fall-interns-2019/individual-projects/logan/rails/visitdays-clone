class CreateSurveyPages < ActiveRecord::Migration[6.0]
  def change
    create_table :survey_pages do |t|
      t.references :survey, null: false, foreign_key: true
      t.integer :page
      t.text :items_json

      t.timestamps
    end
  end
end
