class CreateSurveys < ActiveRecord::Migration[6.0]
  def change
    create_table :surveys do |t|
      t.references :institution, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true
      t.string :name
      t.column :type, :integer, default: 0

      t.timestamps
    end
  end
end
