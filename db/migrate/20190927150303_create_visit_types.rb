class CreateVisitTypes < ActiveRecord::Migration[6.0]
  def change
    create_table :visit_types do |t|
      t.references :institution, null: false, foreign_key: true
      t.string :name
      t.datetime :date_start
      t.datetime :date_end
      t.text :times_json
      t.string :location

      t.timestamps
    end
  end
end
