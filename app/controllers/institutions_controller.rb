class InstitutionsController < ActiveRecordApi::Rest::Controller
  before_action :authenticate_user!, except: :index

  load_and_authorize_resource
  skip_authorize_resource only: :index

  def authorized_models
    unless current_user.present?
      return super
    end

    model_klass.accessible_by(current_ability)
  end
end
