class UsersController < ActiveRecordApi::Rest::Controller
  before_action :authenticate_user!

  load_and_authorize_resource

  def authorized_models
    model_klass.accessible_by(current_ability)
  end
end