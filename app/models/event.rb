class Event < ApplicationRecord
  belongs_to :institution, counter_cache: true

  has_many :event_organizers
  has_many :users, through: :event_organizers

  has_many :registrations
  has_many :registrants, through: :registrations

  resourcify :roles, role_cname: 'Default'
end
