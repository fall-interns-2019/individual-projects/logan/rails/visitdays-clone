# frozen_string_literal: true

class User < ActiveRecord::Base
  rolify role_cname: 'Default'
  # rolify role_cname: 'Member'
  # rolify role_cname: 'Manager'
  # rolify role_cname: 'Admin'
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  include DeviseTokenAuth::Concerns::User

  belongs_to :institution, counter_cache: true, optional: true
  has_many :event_organizers
  has_many :events, through: :event_organizers
  has_many :surveys

  after_create :assign_default_role

  def assign_default_role
    add_role(:default) if roles.blank?
    # create_new_auth_token
  end
end
