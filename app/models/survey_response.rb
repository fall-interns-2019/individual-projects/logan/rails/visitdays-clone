class SurveyResponse < ApplicationRecord
  belongs_to :survey
  belongs_to :registrant

  resourcify :roles, role_cname: 'Default'
end
