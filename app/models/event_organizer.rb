class EventOrganizer < ApplicationRecord
  belongs_to :event
  belongs_to :user

  resourcify :roles, role_cname: 'Default'
end
