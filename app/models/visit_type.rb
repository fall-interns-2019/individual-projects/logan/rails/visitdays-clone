class VisitType < ApplicationRecord
  belongs_to :institution, counter_cache: true

  resourcify :roles, role_cname: 'Default'
end
