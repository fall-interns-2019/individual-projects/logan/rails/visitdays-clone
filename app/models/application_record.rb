class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  after_commit do
    self.class.reflect_on_all_associations(:has_many).each do |association|
      clazz = association.class_name.constantize

      next unless clazz.column_names.include? 'archived'

      foreign_key = association.foreign_key
      clazz.where(foreign_key => id).find_each do |model|
        model.update(archived: archived)
      end
    end
  end
end
