class Survey < ApplicationRecord
  belongs_to :institution, counter_cache: true
  belongs_to :user, counter_cache: true

  has_many :survey_pages
  has_many :survey_responses
  has_many :registrants, through: :survey_responses

  resourcify :roles, role_cname: 'Default'
end
