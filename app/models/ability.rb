# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    alias_action :create, :read, :update, :destroy, to: :crud
    can :read, Institution

    return unless user.present?

    can :crud, VisitType, institution_id: user.institution.id if user.has_role? :manager
    can :manage, :all if user.has_role? :admin

    return unless user.has_role? :member

    can :crud, [Event, Registrant, Registration, Survey],
        institution_id: user.institution.id
    can :read, VisitType, institution_id: user.institution.id
  end
end
