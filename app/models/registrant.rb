class Registrant < ApplicationRecord
  belongs_to :institution, counter_cache: true

  has_many :registrations
  has_many :events, through: :registrations

  has_many :survey_responses
  has_many :surveys, through: :survey_responses

  resourcify :roles, role_cname: 'Default'
end
