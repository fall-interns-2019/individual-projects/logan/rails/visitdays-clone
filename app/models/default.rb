class Default < ApplicationRecord
  has_and_belongs_to_many :users, join_table: :users_defaults


  belongs_to :resource,
             polymorphic: true,
             optional: true


  validates :resource_type,
            inclusion: {in: Rolify.resource_types},
            allow_nil: true

  scopify

  def power
    @power ||= 1
  end
end
