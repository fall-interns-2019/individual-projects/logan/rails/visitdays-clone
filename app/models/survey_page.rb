class SurveyPage < ApplicationRecord
  belongs_to :survey

  resourcify :roles, role_cname: 'Default'
end
