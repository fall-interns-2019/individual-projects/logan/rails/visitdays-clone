class Registration < ApplicationRecord
  belongs_to :registrant
  belongs_to :event

  resourcify :roles, role_cname: 'Default'
end
