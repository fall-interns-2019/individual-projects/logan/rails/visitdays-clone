class Institution < ApplicationRecord
  has_many :events, dependent: :destroy
  has_many :registrants, dependent: :destroy
  has_many :surveys, dependent: :destroy
  has_many :users, dependent: :nullify
  has_many :visit_types, dependent: :destroy

  resourcify :roles, role_cname: 'Default'
end
