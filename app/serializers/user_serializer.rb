class UserSerializer < ActiveModel::Serializer
  attributes %i[created_at email first_name id institution_id last_name provider uid updated_at]
  belongs_to :institution
  has_many :roles
end
