Rails.application.routes.draw do
    mount_devise_token_auth_for 'User', at: 'auth', controllers: {
        token_validations: 'overrides/token_validations'
    }

  root to: 'application#index'

  crud = %i[index show create update destroy]
  resources :institutions, only: crud
  resources :users, only: crud
  resources :visit_types, only: crud
end
